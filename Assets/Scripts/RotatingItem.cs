﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cave;
using AssemblyCSharp.Assets.Scripts;

public class RotatingItem : MonoBehaviour {
    private Quaternion lastWandRotation;
    public bool bRotating = false;
    public GameObject wand;

    public float startRotationAfter = 5.0f;
    private float currTime = 0.0f;

	void Start () {
        wand = GameObject.Find("FlystickSim").gameObject;
	}
	
	void Update () {
        currTime += TimeSynchronizer.deltaTime;
        if (startRotationAfter <= currTime) bRotating = true;
        if (wand)
        {
            if (bRotating)
            {
                Quaternion deltaRotation = lastWandRotation * Quaternion.Inverse(wand.transform.rotation);
                this.transform.rotation = deltaRotation * this.transform.rotation;
            }
            lastWandRotation = wand.transform.rotation;
        } else {
            wand = GameObject.Find("Flystick");
        }
	}

    public float GetTimeToStart(){
        return wand ? Mathf.Max(startRotationAfter - currTime, 0) : 0;
    }
}