﻿using System;
using UnityEngine;
using Cave;

namespace AssemblyCSharp.Assets.Scripts
{
    public class FlystickSim : Flystick
    {
        private float currentRotation;

        void Start(){
            
        }

        void Update()
        {
            currentRotation += TimeSynchronizer.deltaTime * 10;
            this.gameObject.transform.rotation = Quaternion.Euler(currentRotation, currentRotation / 2, 0);
        }
    }
}
