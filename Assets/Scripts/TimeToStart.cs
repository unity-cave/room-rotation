﻿using System;
using UnityEngine;
using UnityEngine.UI;
using Cave;

namespace AssemblyCSharp.Assets.Scripts
{
    public class TimeToStart : MonoBehaviour
    {
        public GameObject from;

        void Start() {
            
        }

        void Update() {
            if(from){
                RotatingItem fromItem = from.gameObject.GetComponent<RotatingItem>();
                float time = fromItem.GetTimeToStart();
                if(time > 0){
                    GetComponent<Text>().text = time.ToString();
                }
                else {
                    this.gameObject.SetActive(false);
                    Destroy(this);
                }
            }
        }
    }
}
